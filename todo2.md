* Get audio file for tune
* Using Python librosa:
  * [https://librosa.org/doc/latest/](Documentation)
  * librosa.feature.chroma_cqt will plot the best guess at a specific pitch (specify tuning)
  * small pitch shifts to ensure best per-measure prediction
  * use number of measures to automatically set measure length
  * try to find the exact measure change near each measure (based on pitch change)
    * shift closest measures some, shift farther measures less
  * track dynamics to determine note vs rest
  * best way to trim outliers?
  * guess each note using smallest expected note size / 2 or /3
  * if adjacent notes of a note center are unexpected (out of tune, wrong note, not same as center or adjacent) then flag that note for manual review
  * (how to deal with ornamentation? or use of full tune structure to inform specific notes?)
  * prepare an ABC file with output

[https://musicinformationretrieval.com/chroma.html](another resource)
