Learn WebGL by copying

- [ ] create basic shapes (circle, cube)
- [ ] display basic shapes on page
- [ ] create matplotlib visualizer for advanced models
- [ ] create template from this stage
- [ ] create more complicated model e.g. trombone
- [ ] is there room for optimization? (min JS, compress models, profiler)
