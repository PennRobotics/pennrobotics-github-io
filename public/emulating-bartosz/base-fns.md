Functions of **base.js**
========================

#### `genericTouchHandler(f)`
TODO

#### `download_file(file_path, handler)`
almost self-explanatory: uses GET to retrieve *file_path* and then calls the function *handler* on the contents

-----

### [https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D](CanvasRenderingContext2D)

It seems a *canvas* element needs to be created to contain the paths created by each function.

#### `CanvasRenderingContext2D.prototype.roundRect = function(x, y, w, h, r)`
draws a rounded rectangle from *x* to *x* + *w* and *y* to *y* + *h* somehow using *arcTo* and *closePath*

#### `CanvasRenderingContext2D.prototype.fillEllipse = function(x, y, r)`
draws a filled ellipse

#### `CanvasRenderingContext2D.prototype.strokeEllipse = function(x, y, r)`
draws an ellipse

#### `CanvasRenderingContext2D.prototype.strokeLine = function(x0, y0, x1, y1)`
draws a line

#### `CanvasRenderingContext2D.prototype.arrow = function(x0, y0, x1, y1, w, arrw, arrh)`
draws an arrow (TODO)

#### `CanvasRenderingContext2D.prototype.feather = function(w, h, l, r, t, b, tx, ty)`
TODO

-----

#### `mat4_transpose(a)`
transpose a 4x4 matrix

#### `mat4_mul(a, b)`
multiply two 4x4 matrices

#### `mat4_mul_vec3(a, b)`
TODO

outputs a 4-element vector

#### `mat4_mul_vec4(a, b)`
TODO

outputs a 4-element vector

#### `mat4_invert(a)`
matrix inversion (4x4)

#### `scale_mat4(a)`
return a matrix scaling by *a* (can be an array or number)

#### `rot_x_mat4(a)`
matrix rotation: x axis

#### `rot_y_mat4(a)`
matrix rotation: y axis

#### `rot_z_mat4(a)`
matrix rotation: z axis

#### `translation_mat4(t)`
create 4x4 translation matrix; inputs a 3-element array (x, y, z translation)

#### `mat3_to_mat4(mat)`
converts a 3x3 rotation matrix to a 4x4 rotation-translation (transformation) matrix

#### `mat4_to_mat3(mat)`
strips translation info from transformation matrix

#### `mat3_invert(a)`
matrix inversion (3x3)

#### `mat3_mul(a, b)`
multiply two 3x3 matrices

#### `mat3_mul_vec(a, b)`
element-wise multiplication of two 3x3 matrices

#### `mat3_transpose(a)`
transpose a 3x3 matrix

#### `scale_mat3(a)`
return a matrix scaling by a number, *a*

#### `rot_x_mat3(a)`
matrix rotation: x axis

#### `rot_y_mat3(a)`
matrix rotation: y axis

#### `rot_z_mat3(a)`
matrix rotation: z axis

#### `rot_aa_mat3(axis, angle)`
TODO

#### `vec_add(a, b)`
element-wise addition of two vectors

#### `vec_sub(a, b)`
element-wise subtraction of vector *b* from vector *a*

#### `vec_scale(a, x)`
scale vector *a* by scalar *x*

#### `vec_mul(a, b)`
element-wise multiplication of two vectors

#### `vec_dot(a, b)`
sum of element-wise multiplication of two vectors (dot product)

#### `vec_cross(a, b)`
cross product of two 3-element vectors

#### `vec_len_sq(a)`
vector length squared (dot product of self)

#### `vec_len(a)`
vector L2 norm

#### `vec_eq(a, b)`
return **true** if *a* has all same element values as *b*, otherwise **false**

#### `vec_norm(a)`
TODO

#### `vec_lerp(a, b, f)`
linear interpolation for each element of two vectors; calls `lerp`

#### `lerp(a, b, f)`
linear interpolation of two numbers

#### `smooth_lerp(a, b, f)`
smooth interpolation of two numbers; cubic from 0 to 1 where slope at beginning and end is 0 and in the middle is 1.5

#### `saturate(x)`
Any *x* below 0 becomes 0. Any *x* greater than 1 becomes 1.

#### `clamp(x, a, b)`
Any *x* below *a* becomes *a*. Any *x* greater than *b* becomes *b*.

#### `step(edge0, x)`
step function: output 1 when *x* is greater than *edge0*, otherwise 0

#### `sharp_step(edge0, edge1, x)`
sloped step function between *edge0* and *edge1* (output 0 to 1, works in both directions)

#### `smooth_step(edge0, edge1, x)`
smooth sloped step function between *edge0* and *edge1*; calls `sharp_step`

#### `rgba255_sq_color(r, g, b, a)`
TODO

#### `rgba255_color(r, g, b, a)`
TODO

#### `flatten(a)`
TODO

-----

*document.addEventListener("DOMContentLoaded", function() { ... })*

creates a variable on the *window* element: *bc_touch_down_state*; adds listeners for various touch events: start, end, cancel; updates the variable during touch events

-----

```
window.TouchHandler = function(target, begin, move, end)
window.Dragger = function(target, callback)
window.SegmentedControl = function(container_div, callback, values)
window.Slider = function(container_div, callback, style_prefix, default_value, disable_click)
window.Shader = function(gl, vert_src, frag_src, attributes_names, uniforms_names)
```
TODO

-----

### `ArcBall(matrix, callback)`
TODO

```
ArcBall.prototype.set_viewport_size = function(width, height)
ArcBall.prototype.set_viewport = function(x, y, width, height)
ArcBall.prototype.start = function(x, y)
ArcBall.prototype.set_matrix = function(m)
ArcBall.prototype.end = function(event_timestamp)
ArcBall.prototype.vec = function(x, y)
ArcBall.prototype.update = function(x, y, timestamp)
```
TODO

-----

### `TwoAxis()`
TODO

```
TwoAxis.prototype.set_size = function(size)
TwoAxis.prototype.set_callback = function(callback)
TwoAxis.prototype.set_horizontal_limits = function(limits)
TwoAxis.prototype.set_vertical_limits = function(limits)
TwoAxis.prototype.start = function(x, y)
TwoAxis.prototype.set_angles = function(angles, continue_velocity)
TwoAxis.prototype.end = function(event_timestamp)
TwoAxis.prototype.update = function(x, y, timestamp)
```
TODO

-----

#### `draw_camera_axes(ctx, l, rot)`
(seems unused)
TODO

#### `compile_shader(gl, source, type)`
(seems unused)
TODO

#### `create_program(gl, vertex, fragment)`
(seems unused)
TODO
