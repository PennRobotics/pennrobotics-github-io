"use strict";

let animated_drawers = [];
let models_ready = false;

(function () {
    const scale = (window.devicePixelRatio || 1) > 1.75 ? 2 : 1;

    let models = {
      /*
      "Model_one_name": {
        "index_offset": 0,
        "index_count": 10,
        "line_index_offset": 10,
        "line_index_count": 15,
      },
      "Model_two_name": {
        "index_offset": 25,
        "index_count": 7,
        "line_index_offset": 32,
        "line_index_count": 4,
      },
      */
    }

    const pi = Math.PI;

    let all_drawers = [];

    function GLDrawer(scale, ready_callback) {
      let canvas = document.createElement("canvas");
      this.canvas = canvas;
      let gl = canvas.getContext('experimental-webgl', { antialias: true });
      // gl.getExtension('');

      let basic_vertex_buffer = gl.createBuffer();
      let basic_index_buffer = gl.createBuffer();

      let vertex_buffer = gl.createBuffer();
      let index_buffer = gl.createBuffer();

      let has_vertices = false;
      let has_indices = false;
      let has_basic = false;

      function mark_ready() {
        if (has_vertices && has_indices && has_basic)  { ready_callback(); }
      }

      /// download_file("vertices.dat", function (buffer) {
      ///     gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
      ///     gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(buffer), gl.STATIC_DRAW);
      ///     gl.bindBuffer(gl.ARRAY_BUFFER, null);
      ///     has_vertices = true;
      ///     mark_ready();
      /// });

      /// download_file("indices.dat", function (buffer) {
      ///     gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_buffer);
      ///     gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(buffer), gl.STATIC_DRAW);
      ///     gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
      ///     has_indices = true;
      ///     mark_ready();
      /// });

      function gen_basic_geometry() {
        let vertices = [];
        let indices = [];

        gl.bindBuffer(gl.ARRAY_BUFFER, basic_vertex_buffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, basic_index_buffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(indices), gl.STATIC_DRAW);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

        has_basic = true;
        mark_ready();
      }

      gen_basic_geometry();

      // Shaders

      null;  /* TODO */
      /// let whatever_shader = new Shader(gl,
      ///     some_instance_transform + some_vert_transform + some_vert_src,
      ///     some_frag_src, ["listparam1", "v_listparam2"], ["m_p1", "m_p2", "color", "p3"]);

      let prev_width, prev_height;
      let viewport_w = 0;
      let viewport_h = 0;
      let line_offset;

      this.set_line_offset = function (x) { line_offset = x; }
      this.begin = function (width, height) {
        line_offset = -0.0007;
        width *= scale;
        height *= scale;

        if (width != prev_width || height != prev_height) {
          canvas.width = width;
          canvas.height = height;
          prev_width = width;
          prev_height = height;
        }

        gl.viewport(0, 0, width, height);

        gl.disable(gl.BLEND);
        gl.depthMask(true);
        gl.depthFunc(gl.LEQUAL);
        gl.clearColor(0.0, 0.0, 0.0, 0.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        gl.disable(gl.CULL_FACE);

        gl.enable(gl.DEPTH_TEST);
        gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

        viewport_w = Math.round(width);
        viewport_h = Math.round(height);

        this.viewport = function (x, y, w, h) { gl.viewport(x*scale, y*scale, w*scale, h*scale); }

        this.finish = function () {
          gl.flush();
          return gl.canvas();
        }

        /// this.draw_whatever = function (name, mvp, rotation, color, opacity, shader_type, params) {}

        this.draw_mesh = function (name, mvp, rotation, color, opacity, shader_type, params) {
          if (color === undefined)
            color = [0.94, 0.94, 0.94, 1];

          let mesh = models[name];

          let shader = simple_shader;  /* Selectively redefine */
          let line_shader = simple_line_shader;  /* Selectively redefine */

          // Draw Lines
          if (mesh.line_index_count > 0) {
            gl.enable(gl.CULL_FACE);
            gl.useProgram(line_shader.shader);
            gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_buffer);

            /* TODO */

            gl.drawElements(gl.TRIANGLES, mesh.line_index_count, gl.UNSIGNED_IN, mesh.line_index_offset * 4);
          }

          gl.useProgram(shader.shader);
          gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
          gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_buffer);

          /* TODO */

          gl.drawElements(gl.TRIANGLES, mesh.index_count, gl.UNSIGNED_IN, mesh.index_offset * 4);
        }
      }
    }

    let gl = new GLDrawer(scale, function () {
        models_ready = true;
        all_drawers.forEach(drawer => {
            drawer.f_draw = true;
            drawer.request_repaint();
        });
    });

    function Drawer(container, mode) {
      let self = this;

      all_drawers.push(self);

      let wrapper = document.createElement("div");
      wrapper.classList.add("canvas_container");
      wrapper.classList.add("non_selectable");

      let canvas = document.createElement("canvas");
      canvas.classList.add("non_selectable");
      canvas.style.position = "absolute";
      canvas.style.top = "0";
      canvas.style.left = "0";

      wrapper.appendChild(canvas);
      container.appendChild(wrapper);

      let play = document.createElement("div");
      play.classList.add("play_pause_button");
      play.onclick = function () { self.set_paused(!self.paused); }

      this.loading_div = document.createElement("div");
      this.loading_div.classList.add("loading_text");
      this.loading_div.textContent = "Loading...";
      container.appendChild(this.loading_div);

      let load_text = mode === "mode1" ||
          mode === "mode2";

      let animated = mode === "mode2" ||
          mode === "mode3";

      let simulated = mode === "mode1" ||
          mode === "mode4";

      let has_reset = mode === "mode3" ||
          mode === "mode2" ||
          mode === "mode6";

      let y_drag_only = mode === "mode5" ||
          mode === "mode6";

      let prev_timestamp;
      let t = 0;
      let width, height;
      let arcball;
      let rot = mat3_mul(rot_x_mat3(2.5), rot_z_mat3(-3.4));  // Default viewpoint, selectively overwrite

      this.paused = true;
      this.requested_repaint = false;
      this.requested_tick = false;

      this.first_draw = true;

      let vp = ident_mat4;
      let proj;
      let ortho_proj;

      function canvas_space(e) {
        let r = canvas.getBoundingClientRect();
        return [(e.clientX - r.left), (e.clientY - r.top)];
      }

      arcball = new ArcBall(rot, function () {
          rot = arcball.matrix.slice();
          request_repaint();
      });

      let sim;  /* TODO */
      let sim_slider;  /* TODO */

      // TODO:
      /// new TouchHandler(canvas, function (e) {...});
      /// ...

      this.set_sim_slider = function (x) { sim_slider = x; }

      this.set_paused = function (p) {
        if (self.paused == p)  { return; }
        self.paused = p;
        if (self.paused) {
          play.classList.remove("playing");
        } else {
          play.classList.add("playing");
          if (!self.requested_tick) {
            self.requested_tick = true;
            window.requestAnimationFrame(tick);
          }
        }
      }

      this.reset = function () {
        sim = [0, 0, 0, 0, 0, 0];  // Selective set
      }

      function tick(timestamp) {
        self.requested_tick = false;

        /* TODO */
      }

      // TODO... if (animated) etc.
    }

    document.addEventListener("DOMContentLoaded", function (event) {
        function make_drawer(name, slider_count, args, sim_slider_index) {
          let ret = [];
          /* TODO */
          return ret;
        }

        make_drawer("hero", 1, [0]);
        make_drawer("hero_movement");
    });
})();

function global_animate(animate) {
  for (var i = 0; i < animated_drawers.length; i++) {
    animated_drawers[i].set_paused(!animate);
  }
  if (animate) {
    document.getElementById("global_animate_on").classList.remove("hidden");
    document.getElementById("global_animate_off").classList.add("hidden");
  } else {
    document.getElementById("global_animate_on").classList.add("hidden");
    document.getElementById("global_animate_off").classList.remove("hidden");
  }
}
