vertfilename = 'vert.dat'
idxfilename = 'idx.dat'
points_offset = 6*654 + 6*126 + 6*2586 + 6*8448 + 6*7044
points_length = 6*756
poly_offset = 349731
poly_length = 6570

import struct
import matplotlib.pyplot as plt
import numpy as np

def main():
  # Read
  with open(vertfilename, 'rb') as file:
    fullrawptdata = file.read()
  print(len(fullrawptdata))
  with open(idxfilename, 'rb') as file:
    fullrawtridata = file.read()
  print(len(fullrawtridata))
  rawptdata = fullrawptdata[4*points_offset : 4*(points_offset+points_length)]
  rawptdata = fullrawptdata  # TODO-debug
  rawtridata = fullrawtridata[4*poly_offset : 4*(poly_offset+poly_length)]
  n = int(len(rawptdata)/4)
  nt = int(len(rawtridata)/4)
  ptdata = struct.unpack("<"+str(n)+"f", rawptdata)
  tridata = struct.unpack(str(nt)+"I", rawtridata)

  # Process
  x = np.array(ptdata[0:n:6])
  y = np.array(ptdata[1:n:6])
  z = np.array(ptdata[2:n:6])
  tri = tridata

  # Display
  ax = plt.figure().add_subplot(projection='3d')
  ax.scatter(x,y,z,s=1.0,marker=',')
  #ax.plot_trisurf(x, y, z, linewidth=0.2, antialiased=True)
  #ax.plot_trisurf(x, y, z, triangles=tri, linewidth=0.2, antialiased=True)

  make_equal(ax,x,y,z)

  if 0:
    plt.show()
  else:
    plt.savefig('out.png', bbox_inches='tight')

def make_equal(ax, x, y, z):
  max_range = np.array([x.max()-x.min(), y.max()-y.min(), z.max()-z.min()]).max() / 2.0
  mid_x = (x.max()+x.min()) * 0.5
  mid_y = (y.max()+y.min()) * 0.5
  mid_z = (z.max()+z.min()) * 0.5
  ax.set_xlim(mid_x - max_range, mid_x + max_range)
  ax.set_ylim(mid_y - max_range, mid_y + max_range)
  ax.set_zlim(mid_z - max_range, mid_z + max_range)
  ax.set_xlim([-15,15])
  ax.set_ylim([-15,15])
  ax.set_zlim([-15,15])
  pass

if __name__ == '__main__':
  main()

# Look at https://matplotlib.org/stable/gallery/mplot3d/trisurf3d.html
# https://webglfundamentals.org/webgl/lessons/webgl-how-it-works.html

# In a simple gl.ARRAY_BUFFER, a rectangle would be created from two triangles
#   in 3d space:
#
#   xA, yA, zA,
#   xB, yB, zB,
#   xC, yC, zC,
#   xB, yB, zB,
#   xD, yD, zD,
#   xC, yC, zC
#
# An example of this is shown:
#
#   A ___ B
#    |  /|
#    | / |
#    |/__|
#   C     D
#
# Because points are reused, an alternative is to define points in an
#   ARRAY_BUFFER and have a separate ELEMENT_ARRAY_BUFFER that links each
#   point to one another:
#
#   xA, yA, zA,
#   xB, yB, zB,
#   xC, yC, zC,
#   xD, yD, zD
#
#   A, B, C,
#   B, D, C
#
# Since faces are usually joined to other faces, many of these points would
#   be duplicated, so they can be referenced in the ELEMENT_ARRAY_BUFFER
#   instead of recreated in the ARRAY_BUFFER.
#
# Matplotlib has similar functionality in the plot_trisurf function (when
#   coupled with the triangles argument).
#
### DEMO
### ====
### x = np.array([20, 0, 20, 0])
### y = np.array([0, 0, 36, 36])
### z = np.array([3, 0, 0, 0,])
### tri = [0,1,2,
###        1,3,2]
### 
### ax = plt.figure().add_subplot(projection='3d')
### ax.plot_trisurf(x, y, z, triangles=tri, linewidth=0.2, antialiased=True)
### ax.set_xlim([-1,40])
### ax.set_ylim([-1,40])
### ax.set_zlim([-1,40])
### 
### plt.show()
