To use the base library, create a function to create and then populate a `<canvas>` element.
In watch.js, this function is named `GLDrawer(scale, ready_callback)`.
A context and extensions are defined for the canvas element, and various buffers are created.
A function is created to check if all of three variables are set: `mark_ready`.
Various routines are called which each end by setting one of the three variables and calling `mark_ready`.
Some of these routines use `download_file` to get more data into a specific buffer.
Once all variables are set, `mark_ready` will call `ready_callback`.
In watch.js, there are more functions which then remove "loading" divs and request repaints of the loaded buffers.
A lot of highly relevant code is found wherever `this.` occurs: `this.begin`, `this.viewport`, `this.finish`, `this.update...buffer`, `this.draw...`, `this.repaint`, etc.
When the page loads, a function `make_drawer` is created to generate a `Drawer` and a `Slider` object for a specified div.
This is then called multiple times to produce each simulation/animation.
There's a `global_animate` function called in the HTML (as well as a few other functions).
Finally, watch.js relies heavily on a function called `draw_complete_assembly` that selectively adds/removes features for various steps.
Each model is named in a dict and has its offset/size stored in a nested dict.
Essentially, each model is loaded into a matrix, a few matrix manipulations are called, and then something like `gl.draw_mesh` is called on the matrix.
