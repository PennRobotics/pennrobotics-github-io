---
title: Homemade Ultra Slim XLR  
author:
- Brian Wright  
date: 2023-08-30  
location: Munich  
tags: [music, crafting]  
category: project  
---
<!-- TODO add to description/summary:
_copying a commercial design for a fixed installation microphone cable_
-->

<!--
## CHECKLIST

- [ ] Can I rip off the DPA DAO 4010?
- [ ] How is the noise rejection?
- [ ] Process
    - [x] Research
    - [x] Part purchase
    - [ ] Part deconstructing
    - [ ] Cobbling together
    - [ ] Measurements
- [ ] Worth it?
    - **update cost tracker during each update!!**
- [ ] Images and other media
-->

## Research

My goal is to create a DIY version of the DPA [DAO4010](https://www.dpamicrophones.com/accessories/microphone-cable-with-slim-xlr-connector-for-pencil-microphone), which is a microphone cable and connector costing about 100 euros that I find impressive for several reasons:

1. It uses an extremely thin cable that is shielded and provides phantom power&mdash;essentially a 10- or 20-meter two-conductor lavalier cable, something like what Alpha Wire would make. (In fact, Mogami makes the wire.)
2. The connector is absurdly low-profile.

### Construction

So far, I have found two types of images of this product. One type is promotional images. The other is from [a Japanese website](https://ameblo.jp/holycater/entry-12591626508.html) where someone analyzes it and takes close-ups of the real product. Here, it seems the front (where the sockets are located) has been gouged out during construction and filled later, which might be a clue into how the thing is made. There's also some surface scratches near the hole with the connector clamp protrudes; I'm not totally sure if those come from production.

One thing is clear, this is a custom job. Neutrik, Hicon, Amphenol and the other manufacturers of XLR connectors do not have a similar connector in their entire catalogs. I'd need to inspect one to figure out exactly what I think is made in-house (e.g. the black block that will hold the sockets) and the parts that they order and then install, like perhaps the sockets that will receive a male XLR connector.

No matter how they make their part, I plan to cut as much as possible from some existing connectors and then possibly to 3d print a connector. We'll see how far that gets us&hellip;


## Purchasing parts

I put in an order for a bunch of connectors so I could cut them apart and attempt to assemble a working prototype:

- [Hicon plug](https://www.thomann.de/intl/hicon_hi_x3cf_b.htm)
- [house-brand plug](https://www.thomann.de/intl/thomann_sk025_stecker.htm)
- [Neutrik installation socket with a connector lock](https://www.thomann.de/intl/neutrik_nc3_fp_b_1.htm)
- [Hicon installation socket with a connector lock](https://www.thomann.de/intl/hicon_hi_x3df_m.htm)
- [4.6mm mic cable](https://www.thomann.de/intl/sommer_cable_the_goblin_red.htm)^[This dimension is diameter; the term _mic cable_ I will use for a cable with shielding plus two signal wires.]
- [4.0mm mic cable](https://www.thomann.de/intl/pro_snake_21000_nf_cable.htm)
- [3.3mm mic cable](https://www.thomann.de/intl/sommer_cable_sc_isopod_so_f22_gy.htm)
- [3.3mm mic cable for digital signals](https://www.thomann.de/intl/sommer_cable_sc_isopod_so_f22.htm)
- [3.0mm mic cable](https://www.thomann.de/intl/cordial_cmk_209_bk.htm)
- [2.6mm mic cable](https://www.thomann.de/intl/sommer_cicada_sod14_sw.htm)

Now, one thing I think this would work really well with is drum kit microphones and particularly toms. There are several really great microphones that latch onto a tom drum rim or stand bracket; really great _and really small_.

- [Audix D2](https://www.thomann.de/intl/audix_d2_drum_microphone.htm)
- [DPA 4099](https://www.dpamicrophones.com/instrument/4099-instrument-microphone?productId=2869)
- [Earthworks DM20](https://www.thomann.de/intl/earthworks_audio_dm20.htm)
- [EV PL 35](https://www.thomann.de/intl/ev_pl_35.htm)
- [Neumann MCM 114](https://www.thomann.de/intl/neumann_mcm_114_set_drums.htm)
- [SE V Beat](https://www.thomann.de/intl/se_electronics_v_beat.htm)
- [Sennheiser E904](https://www.thomann.de/intl/sennheiser_e_904.htm)
- [Sennheiser E604](https://www.thomann.de/intl/sennheiser_e604_evolution.htm)
- [Shure Beta 98](https://www.thomann.de/intl/shure_beta_98_adc.htm)
- and [_so many more_](https://www.thomann.de/intl/microphones_for_toms.html)

What I find interesting is that there is a very wide variety of mounts and cables used by these microphones. The Shure, DPA, and Neumann all have small connectors with long cables. The Earthworks is large with a gooseneck, so the connector can just point down from its mounting bracket. The SE has its connector built in to the mounting bracket. The other four on this list (plus some I didn't list from Lewitt and beyerdynamic) are small and have the connector coming out of the back. Since a tom drum tends to get rocked around quite a bit, it makes sense to minimize the mass attached to the microphone clamp, so something like our proposed DIY connector (or the one sold by DPA) might be a great mate for any of these microphones as long as they still handle noise and rough handling well.

Aside, it's a bit painful realizing that to mic an entire drumset probably would cost you more than the drumset itself. Decent mics, cables, and stands could run 100 to 150 per channel: two for toms, a snare, kick drum, one or two overhead. In addition to cables and mics, you might need more channels on your existing mixer or a small mixer that can input 3 to 8 microphone channels and output a single live-level signal. A 6-mic ANTMIX, 7-mic Mackie, or 8-mic Midas will cost between 200 and 350 euros/dollars.


## Part deconstructing

**TODO**


## Cobbling together a prototype

**TODO**


## Measurements

**TODO**


<!-- 
## Worth it?

Mostly no.

If I discarded the minimum wage for the time I used remaking this design, I would have saved money by just buying 1&ndash;3 of the DPA connectors. That doesn't even include the time I'm using documenting the process.

Was the design process cool? Yeah, it was. It's always nice to take a saw to something and discover how it works.

However, I can't do this discovery every time. It keeps me up late at night which makes me irritable and gives me less time with my family.

If you are looking for a good connector for a boom mic, video mic, drum mic, etc. you have several options&mdash;DPA, of course; Ambient also makes a low-profile XLR connector; or make your own with the knowledge you have found here.
-->

### Total cost

- 32 euros of parts, some that were stored for future use
- 1 hour researching and ordering parts
- 1 hour of cutting open existing connectors
- 0 hours of 3d printing a test connector
- 4 hours of documentation


## Gallery

**TODO**
