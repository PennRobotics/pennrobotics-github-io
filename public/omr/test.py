# TODO: existing solutions include Audiveris, CANTOR?, MusicStaves (Gamera)?, DMOS?, OpenOMR?, Rodan?
# TODO: https://github.com/BreezeWhite/oemer
# MIDISCAN?
# https://machinelearningtutorials.weebly.com/home/i-built-a-music-sheet-transcriber-heres-how
#   ( https://github.com/liuhh02/web-omr )
# https://grfia.dlsi.ua.es/primus/
# http://faculty.washington.edu/garmar/notehead_specs.pdf


# TODO Modules
# potentially: tesseract-ocr, opencv, pandas, numpy, matplotlib, something for pdf?

pixelmax = 255
blksz = 9  # should be odd?
C = 1  # constant to subtract from pixel group

# TODO Best image import? CV2?
im = cv2.imread("input.jpg", cv2.IMREAD_GRAYSCALE)
# optionally, add sharpening in place of this comment
im = cv2.adaptiveThreshold(im, pixelmax, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, blksz, C)
print(cv2.THRESH_BINARY)  # TODO-debug
print(cv2.THRESH_OTSU)  # TODO-debug
# TODO: alternative, calculates optimal parameters
#im = cv2.threshold(im, 0, 255, cv2.THRESH_BINARY|cv2.THRESH_OTSU)

# TODO Fix alignment

# Detect staves

# Break into lines, each containing one staff

# Detect key and time signature, if existing
# Verify with user if a key change or time change is detected

# Identify key and time

# Try to identify measure boundaries

# Identify notes
# Locate each note head
# Calculate pitch by position
# Check if a stem exists
# Check if the stem indicates eighth/sixteenth/etc.
# Check for dot immediately after notehead
# Check for modifier immediately before notehead
# Check for triplets or doublets
# Note position within measure should roughly align with note length
# If note lengths do not add up to correct measure duration, throw an error

# Make a histogram of notes
# Are there outliers? Notify user

# This should be enough information to begin exporting to ABC

