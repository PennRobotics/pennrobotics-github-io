X: 1
T: Mrs. Crotty's Christening
R: reel
C: 15 Feb
O: Day 46
M: 4/4
L: 1/8
K: Dmaj
~d3A BAFA|A2 dA BAFA|BEED E2E2|(3Bcd ef dBAB|
~d3A BAFA|ABdA BAFA|BEED E2 E2|(3Bcd ef d2 AB:|
d2 A~d3 Ad|d2 ef gecA|d2 Bd Adfa|bgef gefe|
d2 A~d3 Ad|d2 ef gecA|d2 A~d3 FA|bgef g2 fe:|
