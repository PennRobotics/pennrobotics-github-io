(function() {
  var body = document.body || document.getElementsByTagName("body")[0];
  var darkButton = document.getElementById("dark-mode");
  darkButton.addEventListener("click", function()
      { if (body.className === "dark")  { body.className = ""; }  else  { body.className = "dark"; } }
  );
})();
