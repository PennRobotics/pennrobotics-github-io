import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
print('import')

y, sr = librosa.load('fergal352.ogg')
print(f'load (sr={sr}, ns={len(y)})')

hlen = 128
C = np.abs( librosa.cqt(y, sr=sr, hop_length=hlen) )

oenv = librosa.onset.onset_strength(y=y, sr=sr, hop_length=hlen)
tgram = librosa.feature.tempogram(onset_envelope=oenv, sr=sr, hop_length=hlen)

tempo, beats = librosa.beat.beat_track(onset_envelope=oenv, sr=sr, hop_length=hlen)
print(f'tempo {tempo}')

fig, ax = plt.subplots(nrows=2, sharex=True)
img = librosa.display.specshow(librosa.amplitude_to_db(C, ref=np.max), sr=sr, x_axis='time', y_axis='cqt_note', ax=ax[0], hop_length=hlen)

db = beats.repeat(2)
eighths = (db[:-1] + db[1:]) // 2

for i, b in enumerate(eighths):
    col = 'w' if i%6==4 else 'y'
    btime = librosa.frames_to_time(b, sr=sr, hop_length=hlen)
    ax[0].axvline(btime, color=col, linestyle='--', alpha=1)

chroma = librosa.feature.chroma_stft(y=y, sr=sr, hop_length=hlen)
imgc = librosa.display.specshow(chroma, y_axis='chroma', x_axis='time', ax=ax[1], hop_length=hlen)

print('disp')
plt.show()

p=librosa.amplitude_to_db(C, ref=np.max)
librosa.midi_to_note(24+40)
np.argsort(p[:,5])[-2]
print('p=librosa.amplitude_to_db(C, ref=np.max)')
print('librosa.midi_to_note(24+40)')
print('np.argsort(p[:,5])[-2]')

# Best results, take the note shortly after each beat
M0 = [np.argsort(p[:,e])[-1] for e in eighths]
M1 = [np.argsort(p[:,e])[-2] for e in eighths]
M2 = [np.argsort(p[:,e])[-3] for e in eighths]

notes = [librosa.midi_to_note(24+v) for v in M0]

