DEBUGGING = False

import re

def DEBUG(dbgstr):
    if DEBUGGING:
        print(dbgstr)

with open('songs-per-page.txt', 'r') as file:
  contents = file.read()

tunes_on_page = [int(tune_count, 10) for tune_count in contents.split('\n')[:-1]]
tune_index = []
for zi_page_num, num_tunes in enumerate(tunes_on_page):
  tune_index += num_tunes * [zi_page_num+1]
DEBUG(tune_index)

with open('song-titles.txt', 'r') as file:
  contents = file.read()[:-1].split('\n')

sort_index = []
for t in contents:
    sort_str = t.lower()
    sort_str = sort_str[sort_str.startswith('the ') and 4:]
    sort_str = sort_str[sort_str.startswith('her ') and 4:]
    sort_str = sort_str[sort_str.startswith('an ') and 3:]
    sort_str = sort_str[sort_str.startswith('a ') and 2:]
    sort_str = re.sub(r'[^ A-Za-z]+', '', sort_str)
    sort_index.append(sort_str)

zipped_real_title = zip(sort_index, contents)
zipped_page_number = zip(sort_index, tune_index)

sorted_z_title = sorted(zipped_real_title)
sorted_z_page = sorted(zipped_page_number)

titles = [elem for _, elem in sorted_z_title]
pages = [elem for _, elem in sorted_z_page]

DEBUG(contents[0:15])
DEBUG(sort_index[0:15])
DEBUG(titles[0:15])
DEBUG(pages[0:15])

out='\n'.join([t + ' ' + (42-len(t))*'.' + ' ' + str(p) for t,p in zip(titles,pages)])
with open('toc-list.txt', 'w') as file:
  print(out, file=file)
