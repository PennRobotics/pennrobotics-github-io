X: 2
T: The Silver Spire
R: reel
C: 5 July
O: Day 186
M: 4/4
L: 1/8
K: Dmaj
FE|:D2 FE DFAc|dcde fdAF|GABG FADF|GFED CEA,C|
D2 FE DFAc|dcde fdAF|GABG FADF|1 EA,CE D2 (3A,B,C:|2 EA,CE D2 CB,||
|:A,B,CD EFGF|~E3F GABc|dcdB ABde|fdgf eA (3BBc|
defd ceAc|dcdB AFDF|GABG FADF|1 EA,CE D2 CB,:|2 EA,CE D2 (3A,B,C||
