X: 1
T: The Sligo
R: reel
C: 31 July
O: Day 212
M: 4/4
L: 1/8
K: Dmaj
|DFAd ~f3e|dcde {g}fdAF|~G2FG EDEF|GABG EFGE|
FADA FAdc|BG~G2 Bcde|~f2af {a}gecd|eddc d4:|
Jf2{g}fe dfaf|{b}gfed cdeg|fa~a2 gfed|cABG A2{B}AG|
FADA FAdc|BG~G2 Bcde|~f2af {a}gecd|eddc d4:|
