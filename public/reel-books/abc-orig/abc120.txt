X: 4
T: The Cuckoo's Nest
R: hornpipe
C: 30 Apr
O: Day 120
M: 4/4
L: 1/8
K: Dmaj
Bc|:d2 dc dfed|c2 A2 A2 AB|=c2 cB cedc|BGAF G2 FG|
AFDF Acde|fdAF G2 FG|ABAG FEDF|1 EDCE D2 Bc:|2 EDCE D2 FG||
|:AFDF AFDF|AGFE D2 EF|GE=CE GECE|GAGF E2 FG|
AFDF Acde|fdAF G2 FG|ABAG FEDF|1 EDCE D2 FG:|2 EDCE D4||
