X: 1
T: Greig's Pipes
R: reel
C: 29 Mar
O: Day 88
M: 4/4
L: 1/8
K: Gmaj
~B2BA BAGA|B2GB AGEG|~B2BA BAGB|cABG AGEG|
~B2BA BAGA|B2GB AGEG|Bd~d2 eBdB|AcBG AGEG||
DG~G2 DGBG|DGBG AGEG|DGGF GABc|d2BG ABGE|
AG~G2 AGBG|DGBG AGEG|DGGF GABc|dBAc BG~G2||
d2 (3Bcd edge|dGBG AGEG|d2 (3Bcd eg~g2|agbg ageg|
d2 (3Bcd edge|dGBG AGEG|d2 (3Bcd eg~g2|agab aged||
