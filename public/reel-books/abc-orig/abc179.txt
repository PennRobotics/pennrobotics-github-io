X: 1
T: The Cat's Meow
R: jig
C: 28 June
O: Day 179
M: 6/8
L: 1/8
K: Dmaj
|:D|FAd fef|afe dBA|BdB BAF|dAF E2 E|
FAd fef|afe dBA|B/c/de fdB|AFE D2:|
|:e|fdB AFA|DFA d2 e|gef ged|ced cBA|
fdB AFA|DFA d2 e|gec age|edc d2:|
|:d|FAd fef|agf agf|Ace g2 g|gfe gfe|fef gfg|
aga bag|1 faa gec|edc d2:|2 fdB gec|dAF D2||
