X: 2
T: The Queen Of Mayo
R: jig
C: 5 Sept
O: Day 248
M: 6/8
L: 1/8
K: Gmaj
|:B3 BAG|EFG ABc|dgg efg|fed cAF|
DBB BAG|EFG ABc|dge fed|1 cAF G2A:|2 cAF G2z||
|:Add df/g/a|agf g3|A^ce efg|gfe f2e|
dAd afa|bgg gfg|age A^ce|1 ed^c d2B:|2 ed^c d2=c||
