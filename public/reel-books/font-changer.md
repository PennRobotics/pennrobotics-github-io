FONTS
=====

## Some basis code (to get started)
```html
<link href='https://fonts.googleapis.com/css?family=A|B|C|D' rel='stylesheet' type='text/css'>
```

## Optimization
https://sia.codes/posts/making-google-fonts-faster/

## Fonts
https://fonts.google.com/specimen/Playfair+Display
https://fonts.google.com/specimen/Raleway
https://fonts.google.com/specimen/Open+Sans
https://fonts.google.com/specimen/Uncial+Antiqua
https://fonts.google.com/specimen/Almendra+Display
https://fonts.google.com/specimen/Almendra
https://fonts.google.com/specimen/Bokor
https://fonts.google.com/specimen/Irish+Grover
https://fonts.google.com/specimen/Dancing+Script
https://fonts.google.com/specimen/Indie+Flower
https://fonts.google.com/specimen/Caveat

## Ideas
https://www.jqueryscript.net/text/Easy-Google-Web-Font-Selector-With-jQuery-Fontselect.html
https://github.com/av01d/fontpicker-jquery-plugin
https://stackoverflow.com/questions/7654708/jquery-changing-font-family-and-font-size
https://stackoverflow.com/questions/32187424/using-jquery-to-change-font-family
https://www.tutorialspoint.com/How-can-I-change-the-font-family-and-font-size-with-jQuery
