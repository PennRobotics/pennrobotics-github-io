%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 4
T: Miss Monaghan
R: reel
C: 10 Nov
O: Day 314
M: 4/4
L: 1/8
K: Dmaj
%%vskip -10
|:D2 (3FED FA A2|BcBA FABc|d2 dB Acde|fede fe e2|
%%vskip -10
D2 (3FED FA A2|BcBA FAAc|d2 dB AcdB|AFEG FD D2:|
%%vskip -10
|:faab afdf|gefd edBc|d2 dB ABde|fede fe e2|
%%vskip -10
fgab afdf|(3gag fd edBc|d2 dB ABde|fede fd d2:|
