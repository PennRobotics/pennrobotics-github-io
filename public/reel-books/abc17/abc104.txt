%%topmargin 0
%%titlespace 0
%%subtitlespace -2
%%composerspace -1
X: 2
T: I Have A House Of My Own
T: With A Chimney Built On Top Of It
R: slip jig
C: 14 Apr
O: Day 104
M: 9/8
L: 1/8
K: Ador
%%vskip -14
|AGE EDE ~E3|AGE EDE GAB|AGE EDE ~E3|EDE GAB AGE|
%%vskip -14
AGE EDE ~E3|AGE EDE GAB|AGE EDE ~E3|EDE GAB AGA|
%%vskip -14
~B3 dBd d3|edB edB dBA|~B3 dBd d2B|AGA B2A GED|
%%vskip -14
~B3 dBd d3|edB edB dBd|e3 dBA B3|AGA B2A GED|
