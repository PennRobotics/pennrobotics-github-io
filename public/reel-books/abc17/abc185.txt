%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 4
T: The Independence
R: hornpipe
C: 4 July
O: Day 185
M: 4/4
L: 1/8
K: Gmaj
%%vskip -22
Bc|:dgfa (3gab gd|(3efg dc (3Bcd BG|FGAB cBce|(3aba (3gfe (3ded (3ABc|
%%vskip -10
dgfa (3gab gd|(3efg dc B2 bg|fedB cAFG|1 (3AAA GF G2 Bc:|2 (3AAA GF G2 z2||
%%vskip -17
|:(3cdc (3BAG (3FAc (3edc|(3ded (3Bdg (3bag (3fed|(3cdc (3BAG (3FAc (3edc|(3ded (3Bdg (3bag (3fed|
%%vskip -10
e2 (3ceg (3c'ba (3gfe|d2 (3Bdg (3bag (3fed|(3cdc (3BAG (3fed (3cBA|1 (3GBd (3gdB G2 AB:|2 (3GBd (3gdB G4||
