%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 1
T: The Tailor's Thimble
R: reel
C: 23 Oct
O: Day 296
M: 4/4
L: 1/8
K: Edor
%%vskip -10
|:GE~E2 E2AF|GE~E2 AFDF|GE~E2 EFGA|1 BcdB AFDF:|2 BcdB ABde||
%%vskip -10
|:f2df efde|fedB ABde|f2df efdA|1 BcdB ABde:|2 BcdB AFDF||
