%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 5
T: Lad O'Beirne's
R: reel
C: 6 May
O: Day 126
M: 4/4
L: 1/8
K: Gmaj
%%vskip -10
|:DG G2 DGBG|BcBG AGEG|DGGF GABd|(3efg dg edge|
%%vskip -10
dB B2 GBdB|cE E2 GE D2|DEGA B2 eB|dBAc BG G2:|
%%vskip -10
|:Bdd^c d2 ef|g2 fg efge|dB B2 GBdB|cE E2 GE D2|
%%vskip -10
Bdd^c d2 ef|g2 fg efge|DEGA B2 eB|dBAc BG G2:|
