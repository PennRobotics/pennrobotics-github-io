%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 1
T: The Old Pigeon On The Gate
R: reel
C: 27 Nov
O: Day 331
M: 4/4
L: 1/8
K: Bbmaj
%%vskip -3
B2 dB fBdB|A2 cA fAcA|B2 dB fBdB |1 edcB AcFA :|2 edcB Ac F2||
%%vskip -10
dfbf dB ~B2|cdef gbgf|dfbf dB ~B2|edcB Ac F2|
%%vskip -10
dfbf dB ~B2|cdef gbgf|dfbf dB ~B2|edcB AcFA:|
