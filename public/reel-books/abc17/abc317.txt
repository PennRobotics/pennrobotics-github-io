%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 1
T: Brigid Of Knock
R: hornpipe
C: 13 Nov
O: Day 317
M: 4/4
L: 1/8
K: Gmaj
%%vskip -10
(3DEF|GFGA Bdgf|agfc ed^cd|BGDG cAFG|ABcB AG (3FED|
%%vskip -10
G,B,D=F ECEG|FDFA GBAc|Bgfd cAFA|G2 GF G2:|
%%vskip -10
|:fg|afdg bgfa|gfge dcBd|ceAc BcdA|FAcA FD (3CB,A,|
%%vskip -10
G,B,D=F ECEG|FDFA GBAc|Bgfd cAFA|G2 GF G2:|
