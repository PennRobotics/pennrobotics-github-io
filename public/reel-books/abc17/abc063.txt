%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 2
T: The Square Of Crossmaglen
R: reel
C: 4 Mar
O: Day 63
M: 4/4
L: 1/8
K: Gdor
%%vskip -5
DGGA BGGA|BGAG FCA,B, CFFE FAcA|FC~C2 A,CFA|
%%vskip -10
DGGA BGGF|~_E3F G#FGA|BABd _ecdB|AFcA BGG2|
%%vskip -10
~g3d Bdga|bgag fcAB|cffe f2ag|fc~c2 Acba|
%%vskip -10
~g3d Bdga|bgag fcAc|~B3G AfcA|BGAF G3d|
