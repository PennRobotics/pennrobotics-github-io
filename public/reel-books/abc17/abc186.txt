%%titlespace -2
%%composerspace 0
%%musicspace 1
X: 2
T: The Silver Spire
R: reel
C: 5 July
O: Day 186
M: 4/4
L: 1/8
K: Dmaj
%%vskip -13
FE|:D2 FE DFAc|dcde fdAF|GABG FADF|GFED CEA,C|
%%vskip -10
D2 FE DFAc|dcde fdAF|GABG FADF|1 EA,CE D2 (3A,B,C:|2 EA,CE D2 CB,||
%%vskip -14
|:A,B,CD EFGF|~E3F GABc|dcdB ABde|fdgf eA (3BBc|
%%vskip -10
defd ceAc|dcdB AFDF|GABG FADF|1 EA,CE D2 CB,:|2 EA,CE D2 (3A,B,C||
