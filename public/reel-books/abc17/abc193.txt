X: 1
T: Rip The Calico
R: reel
C: 12 July
O: Day 193
M: 4/4
L: 1/8
K: Dmaj
|:~d2dc defd|ed (3Bcd egfe|~d2dc defd|efdB AGAB:|
|:dB~B2 gefd|ed (3Bcd egfe|dB~B2 gefd|1 efdB A4:|2 efdB ~A2Ae||
faaf gefd|ed (3Bcd egfe|fa~a2 gefd|efdB ~A2Ae|
faaf gefd|ed (3Bcd egfe|fa~a2 bfaf|~e2ef gefe||
