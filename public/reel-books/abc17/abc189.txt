%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 0
T: Lord McDonald's
R: reel
C: 8 July
O: Day 189
M: 4/4
L: 1/8
K: Amaj
%%vskip -10
EAcA eAcA|d2cd BAFA|EAcA eAcA|EFED CA,A,2|
%%vskip -10
EAcA eAcA|dBcA BAFA|cdcB ABAF|EFAB cAA2||
%%vskip -10
ce~e2 ecAc|efga bgaf|ce~e2 ecAc|BcdB cAA2|
%%vskip -10
ce~e2 ecAc|efga bgaf|ce~e2 ae~e2|dcBd cAA2||
%%vskip -10
eac'a bac'a|ea(3c'ba bafa|eac'a bac'a|efed cAA2|
%%vskip -10
eac'a bac'a|ea(3c'ba bafa|(3c'ba (3bag afe^d|efab c'a~a2||
%%vskip -10
ce(3.e.e.e ecAc|efga bgaf|ce(3.e.e.e ecAc|BcdB cAA2|
%%vskip -10
ce(3.e.e.e ecAc|efga bgaf|ce(3.e.e.e ae(3.e.e.e|dcBd cAA2||
