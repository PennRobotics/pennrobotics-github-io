%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 1
T: The Thrush In The Storm
R: reel
C: 7 July
O: Day 188
M: 4/4
L: 1/8
K: Dmaj
%%vskip -10
FAAF ABde|fgfe dBBA|dBAF dAAF|EAFE DB, B,D|
%%vskip -10
FAAF ABde|fgfe dBBA|dBAF ABde|(3fef ag fdd2:|
%%vskip -10
fedf edBc|defd BAFA|DF ~F2 AF ~F2|fgfd edBd|
%%vskip -10
~f3d egfe|dfed BAGB|~A2FB ABde|(3fef ag fdd2:|
