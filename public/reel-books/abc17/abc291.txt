%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 5
T: The Boys Of The Town
R: jig
C: 18 Oct
O: Day 291
M: 6/8
L: 1/8
K: Gmaj
%%vskip -10
|:ged B2A|BGE G2A|BdB ABA|GBd e2f|
%%vskip -10
ged B2A|BGE G2A|BdB AGA|BGF G3:|
%%vskip -10
|:def gfg|eaa eaa|bag e2d|efg efg|
%%vskip -10
def gfg|afd efg|edB AGA|BGF G3:|
