%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 1
T: The Sally Gardens
R: reel
C: 24 May
O: Day 144
M: 4/4
L: 1/8
K: Gmaj
%%vskip -6
|:G2GA BAGB|dBeB dBAB|d2Bd efge|dBAB GEDE|
%%vskip -10
GFGA BAGB|d2eB dBAB|d2Bd efge|dBAB G4:|
%%vskip -10
|:dggf g2de|g2bg ageg|eaag a2eg|a2bg ageg|
%%vskip -12
dggf g2de|g2bg ageg|d2Bd efge|dBAB G4:|
