%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 2
T: Martin Wynne's
R: reel
C: 12 June
O: Day 163
M: 4/4
L: 1/8
K: Gmaj
%%vskip -10
|:A/B/c|:dged B2Ac|BGEG FA~A2|BG B/c/d cdeg|fgaf gfeB|
%%vskip -10
dged B2Ac|BGEG FA~A2|BG B/c/d cdeg|1 fgaf ~g3B:|2 fgaf ~g3f||
%%vskip -10
|:g3a gfeB|dged BG A/B/c|dggf gfeB|dega bggf|g2af gfeB|
%%vskip -10
dged BG A/B/c|dggf gfeg|1 fgaf ~g3f:|2 fgaf g2||
