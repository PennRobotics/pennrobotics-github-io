%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 1
T: Shandon Bells
R: jig
C: 14 Dec
O: Day 348
M: 6/8
L: 1/8
K: Dmaj
%%vskip -10
AFD DFA|ded cBA|BGE E2G|B2A Bcd|
%%vskip -10
AFD DFA|ded cBA|Bcd ecA|d3 d2B:|
%%vskip -10
|:f2d dcd|f2a afd|cAA eAA|cAc efg|
%%vskip -10
f2d dcd|faa afd|Bcd ecA|d2d d3:|
