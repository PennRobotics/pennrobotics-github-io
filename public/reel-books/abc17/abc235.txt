%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 6
T: The Coleraine
R: jig
C: 23 Aug
O: Day 235
M: 6/8
L: 1/8
K: Amin
%%vskip -10
D|:E2A ABc|Bee e2d|cBA ABc|BEE E2D|
%%vskip -10
E2A ABc|Bee e2d|cBA BA^G|1 A3 A2D:|2 A3 A2B||
%%vskip -10
|:c2c cBA|Bgg g2^g|aed cBA|^GAB E2E|
%%vskip -10
AEA BEB|cde fed|cBA E^FG|1 A3 A2B:|2 A3 A2D||
