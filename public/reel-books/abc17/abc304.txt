%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 9
T: The Haunted House
R: jig
C: 31 Oct
O: Day 304
M: 6/8
L: 1/8
K: Amaj
%%vskip -10
|:A2 A BAB|cAF FEF|Ace f2 e|faf fec|
%%vskip -10
A2 A BAB|cAF FEF|Ace f/g/af|ecB AFE:|
%%vskip -10
Ace f2 e|faf fec|Acd e2 e|efe ecB|
%%vskip -10
Ace f2 e|faf fec|Ace f/g/af|ecB AFE|
%%vskip -10
Ace f2 e|faf fec|Acd e2 e|efe ecB|
%%vskip -10
A2 A BAB|cAF FEF|Ace f/g/af|ecB AFE||
