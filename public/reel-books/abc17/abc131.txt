%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 2
T: Lord Ramsey
R: reel
C: 11 May
O: Day 131
M: 4/4
L: 1/8
K: Gmaj
%%vskip -10
G,2B,D EDB,D|GDB,D EDB,D|G,2B,D EDB,D|GEDB, A,2B,A,|
G,2B,D EDB,D|GDB,D EDB,D|C2EC B,2DB,|1 A,B,CD EDB,A,:|2 A,B,CD EFGB||
%%vskip -10
|:dG~G2 dGeG|dG~G2 FGAB|dG~G2 gfed|cA=FA c2Bc|
dG~G2 dGeG|dG~G2 FGAd|gfed edcB|1 AGFG ABcA:|2 AGFE DCB,A,||
