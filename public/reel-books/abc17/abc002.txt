%%titlespace 0
%%subtitlespace 0
%%composerspace 0
X: 1
T: The Fisherman's Island
R: reel
C: 2 Jan
O: Day 2
M: 4/4
L: 1/8
K: Dmaj
%%vskip -5
D2 FA d2 cA|BAGB AFDF|GEFD EFGA|(3Bcd ed cAAg|
%%vskip -5
fd ~d2 edcA|BcdB AGFD|EFGA (3Bcd ed|1 cAGE FDCE:|2 cAGE D2 z2||
%%vskip -5
|:fd ~d2 AF ~F2|DFAd fdef|ge~e2 bece|dfed cAGE|
%%vskip -5
DF~F2 AddA|B2gf edcB|Adfa gbed|1 cAGE D2 z2:|2 cAGE FDCE||
