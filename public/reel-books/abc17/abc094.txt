%%topmargin 0
%%titlespace 0
%%subtitlespace 0
%%composerspace 0
%%musicspace 1
X: 9
T: Dinny O'Brien's
R: reel
C: 4 Apr
O: Day 94
M: 4/4
L: 1/8
K: Dmix
%%vskip -5
a3g|:fdB/^c/d dcAB|c2Bd cAGE|D2AD E/F/GAB|cBce d^cAg|
%%vskip -5
fdB/^c/d dcAB|c2Bd cAGE|D2AD E/F/GAB|cAGE D3g:|
%%vskip -5
fdB/^c/d defg|a3b agfd|c2cB cdef|g2ga fdfg|a2ag f2ef|
%%vskip -5
edAB cAGE|D2AD E/F/GAB|cAGE D3g:|
