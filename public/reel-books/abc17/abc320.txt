%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 1
T: The Star Above The Garter
R: slide
C: 16 Nov
O: Day 320
M: 12/8
L: 1/8
K: Gmaj
%%vskip -10
d2B BAG ~A3 ABA|G2E c2B BAG ABc|
%%vskip -10
d2B BAG ~A3 ABA|G2E c2E D3 D3:|
%%vskip -10
|:d2e f2a g2e d2B|G2B c2B BAG ABc|
%%vskip -10
d2e f2a g2e d2B|G2B c2E D3 D3:|
