X: 8
T: The Cameronian
R: reel
C: 27 Apr
O: Day 117
M: 4/4
L: 1/8
K: Dmaj
dB|:A2 F>A DAFA|GFEF GBdB|A2 FA DAFA|GBAG F<DDB|
A<F F2 D<F F2|GFEF GBdB|B<AFA DFAF|GBAG F<DDB:|
|:Addc d2 ed|cdef gfed|(3cBA eA fAeA|cdef gfeg|
faeg faeg|f2 fe defg|gf (3gfe fdec|dBAG FD D2:|
