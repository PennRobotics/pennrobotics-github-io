%%titlespace 0
%%composerspace 0
%%musicspace 1
X: 1
T: Na Ceannabháin Bhána
R: slip jig
C: 15 June
O: Day 166
M: 9/8
L: 1/8
K: Gmaj
%%vskip -10
B2 G AGE GED|E/F/GE DEG A2 c|BAG AGE GED|E/F/GE DEF G2 A:|
%%vskip -10
Bd/d/d edB BAG|Bd/d/d edB A3|Bd/d/d edB BAG|GED DEF G3:|
