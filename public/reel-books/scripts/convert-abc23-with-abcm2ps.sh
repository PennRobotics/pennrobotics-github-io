#!/bin/bash

# Exit if required programs are not installed
command -v abcm2ps 1>/dev/null || (echo 'abcm2ps not installed, exiting' && exit 127)
command -v      gs 1>/dev/null || (echo      'gs not installed, exiting' && exit 127)

if [[ ! "`abcm2ps -V`" == *"A4_FORMAT"* ]]; then
	echo "Warning: abcm2ps should be compiled with the A4_FORMAT option, which is probably missing!"
fi

pushd ../abc23;
mkdir -p out;

engrave()
{
	local files=`wc -l abc*.txt | sed '$d' | awk '$1 > 9{print $2}'`

	#local font=Bravura
	#local font=cap1800
	#local font=FinaleMaestro
	#local font=Gootville
	#local font=Legato
	local font=Leipzig
	#local font=Leland
	#local font=Petaluma
	#local font=Sebastian

	local formatters="-F ../scripts/thinlines.fmt -F ../scripts/newheads.fmt"

	local style="--titlefont UncialAntiqua-Regular"

	if [ ! $font = '' ]
	then
		abcm2ps $formatters $style -Oout/master.ps --musicfont $font $files
	else
		abcm2ps $formatters $style -Oout/master.ps $files;
	fi

	# -sPAPERSIZE=a4               \

	gs                            \
	 -sDEVICE=pdfwrite            \
	 -dPDFSETTINGS=/prepress      \
	 -dNOPAUSE                    \
	 -dQUIET                      \
	 -dBATCH                      \
	 -sFONTPATH=../font           \
	 -sOutputFile=out/master.pdf  \
	  out/master.ps
};

engrave;

popd;
