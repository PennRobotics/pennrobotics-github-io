X: 13
T: The Kildare Fancy
R: hornpipe
C: 13 Jan
O: Day 13
M: 4/4
L: 1/8
K: Dmaj
|:d>B|A>FD>F   A>F d>B|(3ABA F>A f>ed>c|B>A  B>f  g>e  f>d |(3efe (3dcB A2 d>B |
      A>FD>F   A>F d>B|(3ABA F>A f>ed>c|B>A  B>f  g>e  f>d | e>c  (3ABc d2 :|
|:d>e|f>dc>d (3Bcd A>F|  D>d c>d f>dc>d|e>A (3AAA f>A (3AAA|(3gfe (3dcB A2 d>e |
      f>dc>d (3Bcd A>F|  D>d c>d f>dc>d|e>A  f>A  g>A  f>A | e>c  (3ABc d2 :|
