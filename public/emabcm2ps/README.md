Emscripten abcm2ps
==================

## Outcome

Success! With no changes:

![Screenshot of inline console on webpage with help text for abcm2ps](no-changes.png)

Delving very shallowly into the code, I tested what happens when changing
`var arguments_ = [''];` to `var arguments_ = ['-V'];`. Upon refreshing the
page, only the version string was displayed on the inline terminal.

_See [abcm2ps.html](https://pennrobotics.github.io/emabcm2ps/abcm2ps.html)_


### Compilation Steps

```sh
$ emconfigure ./configure
$ vi config.h
$ emmake make
$ emcc abcm2ps.o abcparse.o buffer.o deco.o draw.o format.o front.o glyph.o music.o parse.o subs.o svg.o syms.o -lm -o abcm2ps.html
```

The purpose of editing **config.h** is to uncomment the macro for `A4_FORMAT`,
although this could be manually defined in CFLAGS when running `make`.


## Future Work

Since I already have a SHA1-based method of checking if the **abcm2ps**
submodule is current using a GitHub Action, I can change this action to use a
schedule to compile an Emscripten **abcm2ps** when the source changes. An
online ABC converter would be the start of a GitHub Action for converting
from ABC to PostScript on-demand.

[Wasm Ghostscript](https://www.npmjs.com/package/@jspawn/ghostscript-wasm)
seems to exist already, and I'd expect far fewer changes in **GhostScript**
that would affect music engraving than changes in **abcm2ps**. I can either
use that existing package or build my own&mdash;enabling PDF output.


### TODO

- [ ] Create classic project for pennrobotics.github.io repository (grrr.... delete needed!)
- [ ] "Create Appendix with command arguments" as eventual action option
- [ ] Look into these resources
  - [ ] https://pghardy.net/tunebooks/
    - [ ] https://pghardy.net/tunebooks/pgh_session_tunebook_fmt.txt
  - [ ] http://moinejf.free.fr/abcm2ps-doc/pdfmark.xhtml
  - [ ] https://abcplus.sourceforge.net/abcplus_en.pdf
  - [ ] https://www.mcmusiceditor.com/download/Lacerda-decomanual-en.pdf
  - [ ] https://github.com/jawatson/abc-larsen
  - [ ] https://osamc.de/archiv/abcnotation/
  - [ ] https://www.pmwiki.org/wiki/Cookbook/AbcMusic
  - [ ] https://easyabc.sourceforge.net/
