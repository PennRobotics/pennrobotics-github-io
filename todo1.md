The Scahill 2017 Reel Book
==========================

## Stage One

- [x] Document process for getting item titles from master list
- [x] Convert non-ASCII characters
- [x] Script for looking up first result (plus domain keyword)
- [x] System for rating/tracking notation accuracy
- [x] Workarounds for wrong titles, transpositions needed, missing titles, etc.
- [x] Method to quickly identify and save best ABC for each title

## Stage One-and-a-Half

- [x] ❗ Save individual ABC files as text to Github for a full year of daily tunes ❗
- [x] Ensure leap day (either a topical title or the first relatively unique The Session ID not found on 2017 or 2019 lists)

## Stage Two

- [x] Notation display
- [x] Print settings and page break insertion
- [x] Manually adjust scale of individual tunes to fit A4 paper when a tune *almost* fits
- [x] Publish a master PDF output
- [x] Full project documentation (including html section TOC, rearranging sections, responsive when oversized kbd)
- [x] Show info boxes with approximate duration of each step

## Stage Three

- [x] Mention Brother toner workaround and stamp of approval, and add photos showing page quality after official toner cartridge EOL
- [ ] Fix CSS theming (strip away unused, ensure correct grouping and order)
- [ ] Update print CSS to hide non-essential elements but show everything else
- [x] change print style of Day 334 to landscape
- [x] day/date display for Scahill playlist
- [ ] Adjustable settings e.g. font, page size, scale, one or multi per page (plus tweak tool), song style as subheading, ...
- [ ] \* Put all toggles onto their own panel div with a show/hide click region
- [ ] beginner/expert toggle to hide vim explanations
- [x] title TOC in alphabetical order (at end)
- [ ] Show alphabetical index when printing, WITH PAGE NUMS when possible: https://stackoverflow.com/a/60857567/10641561
- [x] buttons to sort by date or alpha
- [x] buttons to group by rhythm or key
- [ ] Alt text
- [ ] Dark mode
- [ ] https://getbootstrap.com/docs/3.3/getting-started/#accessibility
- [ ] Add The Session index number to each tune (field F:, S:, Z:, or N: might work for this)
- [ ] Create histogram of tempos by rhythm type
- [ ] Adjust font props on mobile: Non-SVG font size is much larger than SVG text/tspan font size

\* - https://stackoverflow.com/a/31305343/10641561

## Polishing Stage

- [ ] run bootlint: javascript:(function(){var s=document.createElement("script");s.onload=function(){bootlint.showLintReportForCurrentDocument([]);};s.src="https://stackpath.bootstrapcdn.com/bootlint/latest/bootlint.min.js";document.body.appendChild(s)})();
- [x] change old tags (i, b) to new (em, strong)
- [ ] assign difficulty for each tune
- [ ] add time to calculate difficulty
- [ ] add details about calculating difficulty
- [ ] sort by perceived difficulty
- [ ] check all links and turn written URLs into real links
- [ ] check if min-height unset needs to be outside of the attempt/passint logic
- [ ] try to modify each element's color on the index.html SVG
- [ ] check on various screen sizes, text only, different browsers, etc.
- [x] update final publication date, authorship, credit, etc.
- [ ] add pull-out or tooltip with description of each song style using dl, dt, dd
- [x] escape angle bracket in pre block
- [ ] replace centering style with center-block class
- [ ] hidden-print class?
- [ ] better to squish staves to fit letter-sized paper or adjust margins or ignore side effects?
- [ ] check favicon: [https://realfavicongenerator.net/favicon_checker]()
- [ ] add "copy to clipboard" button for long keyboard entries
- [ ] checkout earlier commit to quickly populate abc-orig subfolder
- [ ] \*\*add reference to Dow's List and parent site
- [X] mention how much quicker this whole thing goes with The Session tune sets (lessons learned)
- [ ] make "Alternate/Variation" text consistent (both in word choice and notation method e.g. `P:`)
- [ ] if not difficult/illegal: steal "two bar preview" on mouseover of search results from The Session, apply to own TOC
- [x] do pre tags ignore or display indentation? can this be cleaned up?
- [ ] convert GDocs spreadsheet to something more palatable, publish when finished
- [x] progress bar during loading
- [ ] after page fully loads, jump to current anchor if the user hasn't scrolled
- [x] TOC into responsive flex columns
- [ ] Other TOC sort/group should be added along with smart styling, hopefully supporting screen readers and text-only browsers (hide all by default, show basic during load with JS, swap in advanced w/ jQuery)
- [ ] Check that the 6th eighth note (an A) is the correct note in **index.html**

\*\* - *http://www.cheakamus.com/Ceilidh/* and *http://www.cheakamus.com/Ceilidh/Downloads/Dows_List.pdf*, also *https://www.mandolincafe.com/forum/archive/index.php/t-142098.html*, *https://thesession.org/discussions/32793*, `site:http://www.cheakamus.com/Ceilidh/Downloads/`, *http://norbeck.nu/abc/TopSessionTunes.asp*, *http://www.gulfweb.net/rlwalker/FrancesIrishGroup/FrancesMichaelsIrishGroupTunes/CollectionsOfIrishTunes/*

(There's a reasonably elegant JS function called `ready_callback` at *view-source:https://ciechanow.ski/js/watch.js* that might help with enforcing script load order and handling promises without using promises or jQuery)
